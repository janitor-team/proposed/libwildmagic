#!/bin/sh

#set -e

workDir=~/devel/packaging/libwildmagic/tarballs

if [ "$#" != "1" ]
then
    echo "Please provide the version number in the form 5p12."
    exit 1
fi

version=${1}

if [ ! -f "WildMagic${1}.zip" ]
then
    echo "File WildMagic${1}.zip not found." 
    exit 1    
fi

newVersion=$(echo ${version} | sed 's/p/\./g')
logFile=${workDir}/libwildmagic+dfsg_${newVersion}.log

cd ${workDir}

rm -vf ${logFile}

echo "Unzipping the WildMagic${1}.zip file." 
unzip WildMagic${version}.zip | tee -a ${logFile}

cd GeometricTools && mv WildMagic5 -v libwildmagic-${newVersion} | tee -a ${logFile}

# Delete all the PDF files
find -name "*.pdf" -exec rm -v {} \; | tee -a ${logFile} 2>&1

find -name "*vcxproj*" -exec rm -vf {} \; | tee -a ${logFile} 2>&1
find -name "*xcodeproj*" -exec rm -rvf {} \; | tee -a ${logFile} 2>&1
find -name "*VC*.sln" -exec rm -vf {} \; | tee -a ${logFile} 2>&1

cd ..
mv GeometricTools/libwildmagic-${newVersion}/ ./
rmdir GeometricTools || exit

echo "Done the preparation of the libwildmagic-${newVersion} source tree"
echo "Now creating the libwildmagic+dfsg_${newVersion}.orig.tar.bz2 archive..."
tar cvjf libwildmagic+dfsg_${newVersion}.orig.tar.bz2 libwildmagic-${newVersion} | tee -a ${logFile} 2>&1
echo "Created the libwildmagic+dfsg_${newVersion}.orig.tar.bz2 archive."