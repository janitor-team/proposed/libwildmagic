#!/bin/sh


# When configuring manually the way the libraries in this project are
# built, using each makeprj.wm5 or makefile.wm5 in each library's
# subdirectory, we need to indicate to the linker the dependencies of
# each library upon the others ones. For example, LibCore does not
# depend on any other library of this project, but LibImagics depends
# on LibCore and LibMathematics, for example, thus, the linker command
# line should include the following:

# -lWm5Core -lWm5Mathematics

# so that when calling objdump on that library the following is
# displayed:

# libWm5Imagics.so
#   NEEDED               libWm5Core.so.5
#   NEEDED               libWm5Mathematics.so.5
#   NEEDED               libstdc++.so.6
#   NEEDED               libm.so.6
#   NEEDED               libgcc_s.so.1
#   NEEDED               libc.so.6

# The way we define which link dependencies each library has, is by
# looking at all the included files and tracking which included files
# are including material from the code of one or the other library.

develDir=$(pwd)

# Test if we are are in the top source dir that should contain debian/

if ! [ -d debian ]
then
    echo "We are not in the top source dir that contains the debian/ dir."
    exit 1
fi

incFiles="all-include-files"
patternList="LibCore LibMathematics LibImagics LibPhysics LibGraphics LibApplications"

# Find all #includes in a set of source files in a given library
# directory. Remove '"': Go in each library directory and issue the
# command:

for libDir in LibCore LibMathematics LibImagics LibPhysics LibGraphics LibApplications
do
    cd ${develDir}/${libDir}
    echo "Processing $(pwd)"
    rm -f ${incFiles}

    for file in $(find -type f)
    do 
        grep '\#include' ${file} | awk '{printf $2 "\n"}' | sed 's/\"//g' >> ${incFiles}
    done 

    # Now remove for each file the CRLF msdos junk.
    cat all-include-files  | tr '\015\012' ' ' > temp
    mv temp all-include-files
    # Remove all the <header> items
    sed -i 's/<[^<>]*>//g' all-include-files
done


# Now that each library directory has a corresponding list of all the
# included files, we can try to determine for each of these included
# files, where they are located in the source directory. This way, we
# can define the code dependencies between each library and the other
# ones.

# For example, let's say we go in LibImagics and we look into the list
# of included files. We'll find, for example that LibImagics, includes
# Wm5Memory.h. If we now go to the top source directory and we $(find)
# that file, we'll see that that file is provided by LibCore. So we
# just determined that LibGraphics depends on LibCore.


for libDir in LibCore LibMathematics LibImagics LibPhysics LibGraphics LibApplications
do
    cd ${develDir}/${libDir} || exit 1
    echo "Processing the dependencies for ${libDir}. The files shown"
    echo "indicate the dependencies of the processed library."
    echo "Press the <Enter> key to continue"
    read answer

    libStringList=""
    for patternDir in ${patternList}
    do 
        if [ ${patternDir} = ${libDir} ]
        then
        continue
        else
            # echo "Adding patternDir to the libStringList: ${patternDir}"
            libStringList="${libStringList} ${patternDir}"
        fi
    done

    # echo "At this point, libStringList is: ${libStringList}"
    
    mv ${develDir}/${libDir}/${incFiles} ${develDir}
    cd ${develDir}
    
    for pattern in ${libStringList}
    do 
        # echo "pattern is: ${pattern}"
        # echo "handling incFiles ${develDir}/${libDir}/${incFiles}"

        for file in $(cat ${incFiles})
        do 
            # echo "file is: ${file}"
            # echo "find ${develDir} -name ${file}"
            find ./ -name ${file} | grep ${pattern}
        done 
    done

done

rm -f ${develDir}/${incFiles}
